#include <libprint/print.hxx>

#include <ostream>

using namespace std;

namespace print
{
  void print_hello (ostream& o, const string& h)
  {
    o << h << endl;
  }
}
