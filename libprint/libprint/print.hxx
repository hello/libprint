#pragma once

#include <iosfwd>
#include <string>

#include <libprint/export.hxx>

namespace print
{
  LIBPRINT_SYMEXPORT void
  print_hello (std::ostream&, const std::string& hello);
}
