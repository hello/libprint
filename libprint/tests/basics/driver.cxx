#include <sstream>

#include <libprint/version.hxx>
#include <libprint/print.hxx>

#undef NDEBUG
#include <cassert>

int main ()
{
  using namespace std;
  using namespace print;

  ostringstream o;
  print_hello (o, "Hello, World!");
  assert (o.str () == "Hello, World!\n");
}
