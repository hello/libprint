// file: libmprint/print.cxx -*- C++ -*-

module print;

#ifdef __clang__
import std.core;
#endif

import std.io;

using namespace std;

namespace print
{
  void
  to_stdout (const string& s)
  {
    cout << s << endl;
  }
}
